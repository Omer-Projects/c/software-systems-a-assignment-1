# Project Path -
PROJECT_PATH=.

# Compiler prefix
CC=gcc -Wall

# cleaners, fixers and lintters
clean:
	rm -rf cmake-build-debug *.o *.out *.a *.so mains maindloop maindrec

# code units
basicClassification.o: $(PROJECT_PATH)/src/basicClassification.c
	$(CC) -c "$(PROJECT_PATH)/src/basicClassification.c" -o basicClassification.o

advancedClassificationLoop.o: $(PROJECT_PATH)/src/advancedClassificationLoop.c
	$(CC) -c "$(PROJECT_PATH)/src/advancedClassificationLoop.c" -o advancedClassificationLoop.o

advancedClassificationRecursion.o: $(PROJECT_PATH)/src/advancedClassificationRecursion.c
	$(CC) -c "$(PROJECT_PATH)/src/advancedClassificationRecursion.c" -o advancedClassificationRecursion.o

mains.o: $(PROJECT_PATH)/src/main.c $(PROJECT_PATH)/src/NumClass.h
	$(CC) -c "$(PROJECT_PATH)/src/main.c" -o mains.o

maindloop.o: $(PROJECT_PATH)/src/main.c $(PROJECT_PATH)/src/NumClass.h
	$(CC) -c "$(PROJECT_PATH)/src/main.c" -o maindloop.o

maindrec.o: $(PROJECT_PATH)/src/main.c $(PROJECT_PATH)/src/NumClass.h
	$(CC) -c "$(PROJECT_PATH)/src/main.c" -o maindrec.o

# libraries
libclassloops.a: basicClassification.o advancedClassificationLoop.o
	ar rcs libclassloops.a basicClassification.o advancedClassificationLoop.o

loops: libclassloops.a

libclassloops.so: basicClassification.o advancedClassificationLoop.o
	$(CC) -shared -fPIC -o libclassloops.so basicClassification.o advancedClassificationLoop.o

loopd: libclassloops.so

libclassrec.a: basicClassification.o advancedClassificationRecursion.o
	ar rcs libclassrec.a basicClassification.o advancedClassificationRecursion.o

recursives: libclassrec.a

libclassrec.so: basicClassification.o advancedClassificationRecursion.o
	$(CC) -shared -fPIC -o libclassrec.so basicClassification.o advancedClassificationRecursion.o

recursived: libclassrec.so

# applications
mains: libclassrec.a mains.o
	$(CC) mains.o libclassrec.a -o mains

maindloop: libclassloops.so maindloop.o
	$(CC) maindloop.o ./libclassloops.so -o maindloop

maindrec: libclassrec.so maindrec.o
	$(CC) maindrec.o ./libclassrec.so -o maindrec

# other utilites scripts
all: loops loopd recursives recursived mains maindloop maindrec
