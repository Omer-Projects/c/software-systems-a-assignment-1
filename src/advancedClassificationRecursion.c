int myPow(int num, int p);

int isArmstrongHelper(int num, int p);
int isPalindromeHelper(int num, int *p);

int isArmstrong(int num)
{
    if (num <= 0)
    {
        return (num == 0) ? 1 : 0; // validation
    }

    int rest = num;

    int p = 0;

    while (rest > 0)
    {
        rest /= 10;

        p++;
    }

    return isArmstrongHelper(num, p) == num;
}

/**
 * private helper
 * return the sum of the nth powers of its digits.
 */

int isArmstrongHelper(int num, int p)
{
    if (num == 0)
    {
        return 0;
    }

    int digit = num % 10;

    return isArmstrongHelper(num / 10, p) + myPow(digit, p);
}

int isPalindrome(int num)
{
    if (num < 0)
    {
        return 0;
    }

    int p = 1;

    return isPalindromeHelper(num, &p);
}

/**
 * private helper
 */
#include <stdio.h>
int isPalindromeHelper(int num, int *p)
{
    if (num < *p)
    {
        *p = (num * 10) < *p ? 1 : 10;
        return 1;
    }

    int digit = num % 10;

    *p = (*p) * 10;

    if (isPalindromeHelper(num / 10, p) == 0)
    {
        return 0;
    }

    *p = (*p) * 100;

    return (((num % (*p / 10)) / (*p / 100)) == digit) ? 1 : 0;
}
