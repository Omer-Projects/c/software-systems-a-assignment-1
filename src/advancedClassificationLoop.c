int myPow(int num, int p);

int isArmstrong(int num)
{
    int rest = num;

    int p = 0;

    while (rest > 0)
    {
        rest /= 10;

        p++;
    }

    rest = num;
    while (rest > 0 && num >= 0)
    {
        int digit = rest % 10;
        rest /= 10;

        num -= myPow(digit, p);
    }

    return num == 0;
}

int isPalindrome(int num)
{
    // validation
    if (num < 0)
    {
        return 0;
    }

    // the only Palindrome that start with '0' is 0
    if (num % 10 == 0)
    {
        return (num == 0) ? 1 : 0;
    }

    // create reverce version of num
    int reverce = 0;

    int rest = num;
    int p = 1;

    while (rest > 9)
    {
        rest /= 10;

        p *= 10;
    }

    rest = num;
    while (rest > 0)
    {
        int digit = rest % 10;
        rest /= 10;

        reverce += digit * p;

        p /= 10;
    }

    // check if reverce same as the number
    return reverce == num;
}
