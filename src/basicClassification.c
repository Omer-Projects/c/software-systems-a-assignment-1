int isPrime(int num)
{
    if (num == 1)
    {
        // Acorrding to the output file 1 is prime
        return 1;
    }

    if (num < 2)
    {
        return 0;
    }

    for (int i = 2; i <= num / 2; i++)
    {
        if (num % i == 0)
        {
            return 0;
        }
    }

    return 1;
}

int isStrong(int num)
{
    int sum = 0;

    int rest = num;
    while (rest > 0)
    {
        int digit = rest % 10;
        rest /= 10;

        int add = 1;
        for (int i = 2; i <= digit; i++)
        {
            add *= i;
        }

        sum += add;
    }

    return num == sum;
}

int myPow(int num, int p)
{
    int ret = 1;

    while (p > 0)
    {
        ret *= num;
        p--;
    }

    return ret;
}
